package si.uni_lj.fri.pbd.lab5;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.lang.ref.WeakReference;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    // TODO: Create timerService and serviceBound\
    private TimerService timerService;
    private boolean serviceBound;
    private Button timerButton;
    private TextView timerTextView;


    // Handler to update the UI every second when the timer is running
     private final Handler mUpdateTimeHandler = new UIUpdateHandler(this);
    // Message type for the handler
     private final static int MSG_UPDATE_TIME = 0;

     private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            Log.d(TAG, "Service bound");

            TimerService.RunServiceBinder binder = (TimerService.RunServiceBinder) iBinder;
            timerService = binder.getService();
            serviceBound = true;
            timerService.background();
            // Update the UI if the service is already running the timer
            if (timerService.isTimerRunning()) {
                updateUIStartRun();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            Log.d(TAG, "Service disconnect");
            serviceBound = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        timerButton = (Button)findViewById(R.id.timer_button);
        timerTextView = (TextView)findViewById(R.id.timer_text_view);
    }

    @Override
    protected void onStart() {
        super.onStart();

        // TODO: uncomment this and write the code to the Service
         Log.d(TAG, "Starting and binding service");
        Intent i = new Intent(this, TimerService.class);
        startService(i);
        // TODO: then uncomment this to bind the Service

         bindService(i, mConnection, 0);
        i.setAction(TimerService.ACTION_START);

    }

    @Override
    protected void onStop() {
        super.onStop();
        updateUIStopRun();

        // TODO: if the Service is bound, unbind it
        if (serviceBound) {
            // If a timer is active, foreground the service, otherwise kill the service
            if (timerService.isTimerRunning) {
                timerService.foreground();
            }
            else {
                stopService(new Intent(this, TimerService.class));
            }
            // Unbind the service
            unbindService(mConnection);
            serviceBound = false;
        }
    }

    public void runButtonClick(View v) {

        // TODO: modify to check whether the service is bound and whether the service's timer is running
        //  and then start/stop the service's timer
        if (!timerService.isTimerRunning()&&serviceBound) {
            Log.d(TAG, "Starting timer");
            timerService.startTimer();
            updateUIStartRun();
        } else if(timerService.isTimerRunning()&&serviceBound){
            Log.d(TAG, "Stopping timer");
            timerService.stopTimer();
            updateUIStopRun();
        }
    }

    /**
     * Updates the UI when a run starts
     */
    private void updateUIStartRun() {
        // TODO: Uncomment to start periodic UI updates
        mUpdateTimeHandler.sendEmptyMessage(MSG_UPDATE_TIME);
        timerButton.setText(R.string.timer_stop_button);
    }

    /**
     * Updates the UI when a run stops
     */
    private void updateUIStopRun() {
        // TODO: Uncomment to stop periodic UI updates
         mUpdateTimeHandler.removeMessages(MSG_UPDATE_TIME);
        timerButton.setText(R.string.timer_start_button);
    }

    /**
     * Updates the timer readout in the UI; the service must be bound
     */
    private void updateUITimer() {
        if(serviceBound) {
            timerTextView.setText(timerService.elapsedTime() + " seconds");
        }

    }

    /**
     * When the timer is running, use this handler to update
     * the UI every second to show timer progress
     */
    // TODO: Uncomment the handler to get periodic UI updates
     static class UIUpdateHandler extends Handler {

        private final static int UPDATE_RATE_MS = 1000;
        private final WeakReference<MainActivity> activity;

        UIUpdateHandler(MainActivity activity) {
            this.activity = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message message) {
            if (MSG_UPDATE_TIME == message.what) {
                Log.d(TAG, "updating time");
                activity.get().updateUITimer();
                sendEmptyMessageDelayed(MSG_UPDATE_TIME, UPDATE_RATE_MS);
            }
        }
    }
}

